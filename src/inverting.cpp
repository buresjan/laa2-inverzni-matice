#include "../hdr/inverting.h"
#include <cstdlib>

void PrintMatrix(double** array, int row, int col) {    //Funkce na vypsani zadane matice
    double **matrix = array;
    cout << "\n=== Matrix ===\n";

    for (int i = 0; i < row; i++) {
        for (int j = 0; j < col; j++) {
            cout << matrix[i][j] << "  ";
        }
        cout << "\n";
    }
}

void InverseOfMatrix(double** matrix, int order)    //Funkce na invertovani matice
{
    double** array = matrix;
    double temp;

    // Zde vypisujeme zadanou matici
    PrintMatrix(matrix, order, order);

    //Vytvorime 2D pole reprezentujici rozsirenou matici o jednickovou matici - vytvarim zde lokalni pole, protoze mi v prubehu delalo potize pracovat s pointery
    double m [order][2 * order];
    for (int i = 0; i < order; i++) {
        for (int j = 0; j < order; j++) {
            m [i][j] = array[i][j];
        }
        for (int k = 0; k < order; k++) {
            if (i == k) {
                m[i][k + order] = 1;
            }
            else {
                m[i][k + order] = 0;
            }
        }
    }

    //Dostaneme na prvek 11 nenulove cislo, pokud existuje
    for (int i = order - 1; i > 0; i--) {
        if (abs(m[i - 1][0]) < abs(m[i][0]))
            for (int j = 0; j < 2 * order; j++) {
                temp = m[i][j];
                m[i][j] = m[i - 1][j];
                m[i - 1][j] = temp;
            }
    }

    for (int i = 0; i < order; i++) {
        for (int j = 0; j < order; j++) {
            if (j != i) {
                //Prohodime radky tak, aby na pozici ii nebyla nula, pokud to nejde, projevi se to na konci programu
                //Nasledne vzdy i-ty radek odecteme od ostatnich tak, aby v celem sloupci vznikly nuly, rom pozice ii
                if (m[i][i] == 0 && i != order - 1) {
                    for (int t = order - 1; t > i; t--) {
                        if (abs(m[t - 1][i]) < abs(m[t][i]))
                            for (int r = 0; r < 2 * order; r++) {
                                temp = m[t][r];
                                m[t][r] = m[t - 1][r];
                                m[t - 1][r] = temp;
                            }
                    }
                    temp = m[j][i] / m[i][i];
                    for (int k = 0; k < 2 * order; k++) {
                        m[j][k] -= m[i][k] * temp;
                        if (m[j][k] > -10e-15 && m[j][k] < 10e-15){
                            m[j][k] = 0;
                        }
                    }
                }
                else if (m[i][i] == 0 && i == order -1) {}
                else {
                    temp = m[j][i] / m[i][i];
                    for (int k = 0; k < 2 * order; k++) {
                        m[j][k] -= m[i][k] * temp;
                        if (m[j][k] > -10e-15 && m[j][k] < 10e-15){
                            m[j][k] = 0;
                        }
                    }
                }
            }
        }
    }

    //Kazdy radek vydelime tak, aby v diagonale v leve strane matice vznikly same jednicky
    for (int i = 0; i < order; i++) {
        temp = m[i][i];
        for (int j = 0; j < 2 * order; j++) {
            m[i][j] = m[i][j] / temp;
        }
    }

    //Overime zda nam na leve strane vznikla jednotkova matice
    int check = 0;
    for (int i = 0; i < order; i++) {
        if (m[i][i] != 1){
            check++;
        }
    }

    //Pokud proces probehl spravne, vypiseme nalezenou inverzni matici. Jinak -> chybova hlaska
    if (check == 0){
        cout << "\n=== Inverse Matrix ===\n";
        for (int i = 0; i < order; i++) {
            for (int j = 0; j < order; j++) {
                cout << m[i][j + order] << "  ";
            }
            cout << "\n";
        }
    }
    else {
         cout << "Chyba - zadana matice neni regularni, neexistuje k ni matice inverzni";
    }
}