#include "../hdr/main.h"

int main()
{
    cout << "\nMatice k vypoctu se nachazi ve slozce matrix_input v soboru input.txt. Pro zmenu matice k vypoctu vlozte do tohoto souboru matici k vypoctu ve spravnem tvaru.\n";
    dim order = GetDimensions();   //Do structu nacteme rozmery matice

    if (order.r == 0 && order.c == 1) { //Osetrime pripad, pokud je v matici pouze jeden prvek - pracujeme s predpokladem, ze je v soboru alespon jeden prvek je
        order.r ++;
    }
    if (order.r == order.c) {   //Pokud je matice ctvercova, zacne proces invertovani
        double** matrix = GetInput(order.r, order.c);
        InverseOfMatrix(matrix, order.r);
    }
    else {  //Pokud matice neni ctvercova -> chybova hlaska
        cout << "Chyba - zadejte matici ve spravnem tvaru, musi byt ctvercova.\n";
    }

    return 0;
}
