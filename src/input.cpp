#include "../hdr/input.h"

dim GetDimensions(){   //Funkce na ziskani rozmeru matice
    int col = 0, row = 1;
    int temp = 0;
    char curr, prev = 0;
    ifstream file("../matrix_input/input.txt");

    //Zde je pocitan pocet radku a sloupcu v souboru
    do {
        file.get(curr);
        if ((temp != 2) && (curr == ' ' || curr == '\n') && (prev != ' ' && prev != '\n')) {    //Zabranuje pricteni extra radku, kdyz konci nikoliv cislem, ale mezernikem
            col++;
        }
        if (curr == '\n' && curr != prev) {
            temp = 2;
            row++;
        }
        prev = curr;
    } while (file);

    dim dimensions = {row, col};
    return dimensions; //Vrati struct s rozmery matice
}

double **GetInput(int row, int col){   //Funkce na nacteni matice

    ifstream file("../matrix_input/input.txt");
    file.clear();
    file.seekg(0, ios::beg);

    //Zde dynamicky alokujeme 2D pole na zaklade informace o radu matice
    auto **arr = new double *[row];
    for (int i = 0; i < col; i++) {
        arr[i] = new double[col];
    }
    //Zde ukladame do pole nactena cisla ze souboru
    for (int i = 0; i < row; i++) {
        for (int k = 0; k < col; k++) {
            file >> arr[i][k];
        }
    }
    return arr;
}
