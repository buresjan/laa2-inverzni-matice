# LAA2 - Inverzni matice

Program slouží k výpočtu inverzní matice ze zadané vstupní matice.

**Vstup** je soubor jménem `input.txt` ve složce `matrix_input`, která je součástí repozitáře. Pro změnu vstupní matice stačí přepsat obsah toho souboru.
**Formát vstupu** je čtvercová matice, v které jsou čísla oddělena mezerníkem a jednotlivé řádky jsou odděleny enterem. Příkladem takového vstupu je vzorový vstup 

1 0 8

2 4 1

1 2 3



který je použit na vstupu, pokud uživatel nedá jinak.

**Pozor** - Pro správnou funkci programu musí sobour obsahovat alespoň jednu číslici.

Ke správné funkci programu jsou nutné obě složky `hdr` i `src` (ve které se nachází i hlavní zdrojový kód `main.cpp`). 

Výsledná inverzní matice je uživateli vypsána do konzole.